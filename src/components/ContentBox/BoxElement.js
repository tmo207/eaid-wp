import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
  DARKBLUE_BG,
  LIGHTBLUE_BG,
  WHITE,
  ROUNDED_CORNERS,
  HANDHELD_MQ
} from '../../_common/config';

const Wrapper = styled.div`
  overflow: hidden;
  border-radius: ${ROUNDED_CORNERS};
  background: ${props => (props.lightBG ? LIGHTBLUE_BG : DARKBLUE_BG)};
  padding: ${props => (props.noPadding ? '0' : '2rem')};
  margin-bottom: ${props => (props.noBorder ? '0' : '0.06rem')};
  flex-grow: ${props => (props.flexGrow ? props.flexGrow : '1')};
  display: ${props => (props.inline ? 'inline-block' : 'flex')};
  flex-wrap: ${props => (props.wrap ? 'wrap' : 'nowrap')};
  color: ${props => (props.lightBG ? '#000000' : WHITE)};
  justify-content: ${props => props.justify};
  margin: ${props => props.margin};
  flex-direction: ${props => (props.column ? 'column' : 'row')};

  @${HANDHELD_MQ} {
    padding: ${props => (props.noPadding ? '0' : '1rem')};
  }
`;

const BoxElement = ({
  children,
  noBorder,
  flexGrow,
  wrap,
  noPadding,
  lightBG,
  justify,
  inline,
  id,
  margin,
  column
}) => (
  <Wrapper
    id={id}
    noPadding={noPadding}
    noBorder={noBorder}
    flexGrow={flexGrow}
    wrap={wrap}
    lightBG={lightBG}
    justify={justify}
    inline={inline}
    margin={margin}
    column={column}
  >
    {children}
  </Wrapper>
);

BoxElement.propTypes = {
  children: PropTypes.node.isRequired,
  noBorder: PropTypes.bool,
  flexGrow: PropTypes.number,
  wrap: PropTypes.bool,
  noPadding: PropTypes.bool,
  lightBG: PropTypes.bool,
  justify: PropTypes.string,
  inline: PropTypes.bool,
  id: PropTypes.string,
  margin: PropTypes.string,
  column: PropTypes.bool
};

export default BoxElement;
